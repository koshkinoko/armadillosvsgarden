﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AdditionalPagesController : MonoBehaviour
{
    [SerializeField] private Text _text;

    public void onBackClick()
    {
        SceneManager.LoadScene("Menu");
    }
}
