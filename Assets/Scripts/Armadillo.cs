﻿using UnityEngine;

public class Armadillo : MonoBehaviour
{
    public bool mortal = false;
    public int lifes;
    private readonly Color[] _colors = {Color.red, new Color(255, 168, 0), Color.white};
    [SerializeField] private int _minimalForce = 10;
    [SerializeField] private int _minimalTorque = 10;
    
    private float _weakEnemyChance;
    private float _mediumEnemyChance;
    private const float MaxWeakEnemyChance = 0.5f;
    private const float MaxMediumEnemyChance = 0.8f;
    private const float MinWeakEnemyChance = 0.05f;
    private const float MinMediumEnemyChance = 0.45f;
    
    [SerializeField, HideInInspector] protected Rigidbody Rigidbody;
    private Material _material;


    private void OnEnable()
    {
        updateEnemyChance();
        Rigidbody = GetComponent<Rigidbody>();
        var enemyWeakness = Random.Range(0, 1.0f);
        if (enemyWeakness < _weakEnemyChance)
        {
            lifes = 1;
        }
        else if(enemyWeakness < _mediumEnemyChance)
        {
            lifes = 2;
        }
        else
        {
            lifes = 3;
        }
        Rigidbody.AddRelativeForce(new Vector3(1, 0, -0.4f) * _minimalForce * lifes, ForceMode.Impulse);
        Rigidbody.AddRelativeTorque(new Vector3(1, 0, -0.4f)  * _minimalTorque * lifes, ForceMode.Impulse);
        _material = GetComponent<Material>();
        UpdateColor();
    }
    
    private void updateEnemyChance()
    {
        var koef = GameManager.Instance.SecondsLeft * 1.0f / (GameManager.Instance.Seconds * 1.0f);
        _weakEnemyChance = (MaxWeakEnemyChance - MinWeakEnemyChance) * koef + MinWeakEnemyChance;
        _mediumEnemyChance = (MaxMediumEnemyChance - MinMediumEnemyChance) * koef + MinMediumEnemyChance;

    }
    
    private void UpdateColor()
    {
        if (_material == null) return;
        var color = _colors[lifes];
        _material.color = color;
    }

    private void OnMouseDown()
    {
        if(!mortal) return;
        lifes--;
        if (lifes <= 0)
        {
            GameManager.Instance.AddScore(70);
            Destroy(gameObject);
            return;
        }

        GameManager.Instance.AddScore(25);
        UpdateColor();
    }
}