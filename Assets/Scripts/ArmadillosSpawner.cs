﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmadillosSpawner : MonoBehaviour
{
    public float SpawnTime = 1f;

    public Vector3 MinOffset;
    public Vector3 MaxOffset;
    public GameObject Armadillo;

    private void OnEnable()
    {
        StartCoroutine(SpawnRoutine());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private IEnumerator SpawnRoutine()
    {
        while (isActiveAndEnabled)
        {
            yield return new WaitForSeconds(SpawnTime);
            Spawn();
        }
    }
	
    private void Spawn()
    {
        var prefab = Armadillo;
        if (!prefab) 
            return;		
        var position = new Vector3(-35, -6, Random.Range(-20, -5));
//            GameManager.Instance.ViewportToWorld(new Vector3(
//            Random.Range(MinOffset.x, MaxOffset.x), 
//            2f,
//            Random.Range(MinOffset.z, MaxOffset.z)));
        Instantiate(prefab, position, Quaternion.identity);
    }
}
