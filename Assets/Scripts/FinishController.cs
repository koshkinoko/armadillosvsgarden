﻿using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class FinishController : MonoBehaviour
{
    [SerializeField] private Text _scoresText;
    [SerializeField] private Text _bonusScoresText;
    [SerializeField] private Text _summaryScoresText;
    [SerializeField] private Text _headerText;
    [SerializeField] private int _scoreAnimationTime;
    private int _scores;
    private int _bonusScores;
    private int _summaryScores;

    private const string LooserText = "САД УНИЧТОЖЕН БЕЗЖАЛОСТНЫМИ БРОНЕНОСЦАМИ!";
    private const string WinnerText = "ВЫ УДЕРЖАЛИ САД ДО ПРИЕЗДА ДЯДИ БЕНА!";

    private void Start()
    {
        _scores = ScoresKeeper.Instance().Scores;
        _bonusScores = ScoresKeeper.Instance().BonusScores;
        _summaryScores = ScoresKeeper.Instance().SummaryScores;
        _headerText.text = _bonusScores == 0 ? LooserText : WinnerText;
        UpdateScores();
    }

    private void UpdateScores()
    {
        if (_scores == 0) return;
        var animTime = (int) (_scoreAnimationTime / (_summaryScores * 2.0) * _scores);
        float deltaTime = 0f;
        _scoresText.DOFade(1f, animTime).OnUpdate(() =>
        {
            deltaTime += Time.deltaTime;
            var newTime = (int) (_scores * deltaTime / animTime);

            _scoresText.text = $"{newTime}";
        }).OnComplete(() =>
        {
            _scoresText.text = $"{_scores}";
            animTime = (int) (_scoreAnimationTime / (_summaryScores * 2.0) * _bonusScores);
            deltaTime = 0f;
            _bonusScoresText.DOFade(1f, animTime).OnUpdate(() =>
            {
                if (_bonusScores == 0) return;
                deltaTime += Time.deltaTime;
                var newTime = (int) (_bonusScores * deltaTime / animTime);

                _bonusScoresText.text = $"{newTime}";
            }).OnComplete(() =>
            {
                _bonusScoresText.text = $"{_bonusScores}";
                animTime = (int) (_scoreAnimationTime / 2.0);
                deltaTime = 0f;
                _summaryScoresText.DOFade(1f, animTime).OnUpdate(() =>
                {
                    deltaTime += Time.deltaTime;
                    var newTime = (int) (_summaryScores * deltaTime / animTime);

                    _summaryScoresText.text = $"{newTime}";
                }).OnComplete(() =>
                { 
                    _summaryScoresText.text = $"{_summaryScores}";
                });
            });
        });
    }

    public void onMenuClick()
    {
        SceneManager.LoadScene("Menu");
    }
}