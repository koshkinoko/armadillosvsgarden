﻿using System.Collections;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private int _scores;
    public Text ScoreText;
    public int ApplyBonusOn = 1000;
    public GardenLifeController GardenLife;
    [SerializeField] private int _lifes = 12;
    public Text LifesText;
    [SerializeField] private Text _countDown;
    [SerializeField] private int _seconds;
    private int _secondsLeft;
    

    public static GameManager Instance { get; private set; }

    public int Seconds
    {
        get { return _seconds; }
    }

    public int SecondsLeft
    {
        get { return _secondsLeft; }
    }

    private void Awake()
    {
        Instance = this;
    }


    // Start is called before the first frame update
    private void Start()
    {
        AddScore(0);
        _secondsLeft = Seconds;
        UpdateSeconds();
        StartCoroutine(WaitForFinishGame());
    }


    private IEnumerator WaitForFinishGame()
    {
        while (SecondsLeft >= 0)
        {
            yield return new WaitForSeconds(1f);
            UpdateSeconds();
        }
    }


    private void UpdateSeconds()
    {
        _countDown.text = $"{SecondsLeft / 60}:{SecondsLeft % 60:00}";
        _secondsLeft = SecondsLeft - 1;
        if (SecondsLeft < 0)
        {
            GameOver();
        }
    }

    public void AddScore(int value)
    {
        var newScore = _scores + value;
        if (newScore / ApplyBonusOn > _scores / ApplyBonusOn)
        {
            ApplyLifeBonus();
        }

        _scores = newScore;


        ScoreText.text = _scores.ToString();
    }

    public void SubtractLife()
    {
        _lifes--;
        LifesText.text = _lifes.ToString();
        if (_lifes <= 0)
        {
            GameOver();
        }
    }

    private void ApplyLifeBonus()
    {
        _lifes++;
        LifesText.text = _lifes.ToString();
    }

    private void GameOver()
    {
        ScoresKeeper.Instance().UpdateScores(_scores, (int) (_lifes / 10.0 * _scores));
        SceneManager.LoadScene("Finish");
//        FinishController.Instance.SetScores(_scores);
    }
}