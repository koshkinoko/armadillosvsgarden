using UnityEngine;

namespace DefaultNamespace
{
    public class GardenLifeController : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            GameManager.Instance.SubtractLife();
        }
    }
}