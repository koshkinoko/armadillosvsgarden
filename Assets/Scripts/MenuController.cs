﻿using DefaultNamespace;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    private int _bestScores;
    private readonly string _bestScoresTextPattern = "ЛУЧШИЙ СЧЁТ: {0}";
    [SerializeField] private Text _bestScoresText;
    
    // Start is called before the first frame update
    private void Start()
    {
        _bestScores = ScoresKeeper.Instance().BestScores;
        _bestScoresText.text = string.Format(_bestScoresTextPattern, _bestScores);
    }

    public void OnPlayClick()
    {
        SceneManager.LoadScene(_bestScores == 0 ? "Story" : "Game");
    }

    public void OnRulesClick()
    {
        SceneManager.LoadScene("Rules");
    }

    public void OnAboutClick()
    {
        SceneManager.LoadScene("About");
    }
}