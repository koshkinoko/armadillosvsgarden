namespace DefaultNamespace
{
    public class ScoresKeeper
    {
        private static ScoresKeeper _instance;

        public int Scores { get; private set; }
        
        public int BonusScores { get; private set; }
        
        public int SummaryScores { get; private set; }

        public int BestScores { get; private set; }

        public static ScoresKeeper Instance()
        {
            if (_instance == null)
            {
                _instance = new ScoresKeeper();
            }

            return _instance;
        }

        public void UpdateScores(int scores, int bonusScores)
        {
            Scores = scores;
            BonusScores = bonusScores;
            SummaryScores = scores + bonusScores;
            if (SummaryScores > BestScores)
            {
                BestScores = SummaryScores;
            }
        }
    }
}