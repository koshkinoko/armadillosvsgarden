using UnityEngine;

namespace DefaultNamespace
{
    public class ShootAreaController : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            var armadillo = other.GetComponentInParent<Armadillo>();
            if (armadillo != null)
            {
                armadillo.mortal = true;
            }
        }
    }
}