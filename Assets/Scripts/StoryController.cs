﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StoryController : MonoBehaviour
{
    private Speech[] _speeches = {
        new Speech("Ах, мои клумбы! Кто это сделал?", 2), 
        new Speech("Мы с моими друзьями играли в \"Сломай клумбу\". Кто сломает больше клумб, тот и победил.", 1),
        new Speech("Оставьте в покое мой садик! Тут растут мои любимые цветы, зачем вы их портите?", 2),
        new Speech(" Уходите прочь, или я...", 2),
        new Speech("Что? Ты смеешь нам угрожать? Мы клан гигантских броненосцев, все падают ниц перед нашим войском!", 1),
        new Speech("Глупая девчонка, ты ничего не сможешь сделать против нас, хе-хе-хе!", 1),
        new Speech("Скоро приедет дядя Бен, посмотрим, как вы тогда запоёте!", 2)};

    [SerializeField] private Text _speechText;
    [SerializeField] private Image _speechLeft;
    [SerializeField] private Image _speechRight;
    private int leftId = 1;
    private int _currentSpeech;

    private bool locked;


    // Start is called before the first frame update
    void Start()
    {
        ChangeSpeech();
    }

    // Update is called once per frame
    void Update()
    {
        if (!locked && Input.anyKey)
        {
            ChangeSpeech();
            locked = true;
        }

        if (locked && !Input.anyKey)
        {
            locked = false;
        }
    }

    public void OnSkipClick()
    {
        LoadGame();
    }

    private void ChangeSpeech()
    {
        if (_speeches.Length > _currentSpeech)
        {
            var speech = _speeches[_currentSpeech++];
            _speechText.text = speech.Text;
            if (leftId == speech.PersonId)
            {
                _speechLeft.enabled = true;
                _speechRight.enabled = false;
            }
            else
            {
                _speechLeft.enabled = false;
                _speechRight.enabled = true;
            }
        }
        else
        {
            LoadGame();
        }
    }

    private void LoadGame()
    {
        _currentSpeech = 0;
        SceneManager.LoadScene("Game");
    }

    private class Speech
    {
        private string _text;

        private int _personId;

        public Speech(string text, int personId)
        {
            _text = text;
            _personId = personId;
        }

        public string Text => _text;

        public int PersonId => _personId;
    }
}